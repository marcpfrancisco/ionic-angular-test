## Ionic Angular Test Project

> **I created this repo for the sole purpose of Creating / Testing certain feautures
> in Ionic made with Angular.**

### Dependencies

- Angular
- Ionic
- Cordova

### To install simply do:

```
npm install
--
ionic serve
```

<p>All rights reserved 2020 <br>
&copy; Marc Piolo F. Francisco ❤️
