import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HomepageService } from "../services/homepage.service";

@Component({
	selector: "app-home",
	templateUrl: "home.page.html",
	styleUrls: ["home.page.scss"],
})
export class HomePage {
	homepageForm: FormGroup;
	_users: any = [];
	_posts: any = [];
	_comments: any = [];

	noUserId: boolean = false;
	noPostId: boolean = false;

	constructor(
		private _fb: FormBuilder,
		private _homeService: HomepageService
	) {}

	ngOnInit() {
		this._homeService.getAllUsers().subscribe(
			(res: any) => {
				this._users = res;
			},
			(error) => (this._users = [])
		);

		this.createHomePageForm();
	}

	createHomePageForm() {
		this.homepageForm = this._fb.group({
			users: [null, Validators.required],
			posts: [null, Validators.required],
			comments: [null, Validators.required],
		});
	}

	get f() {
		return this.homepageForm.controls;
	}

	getPostByUserId(userId: number) {
		if (userId) {
			this._homeService.findPostByUserId(userId).subscribe(
				(res: any) => {
					this._posts = res;
					this._comments = null;
				},
				(error) => console.log(error)
			);
		} else {
			this.noUserId = true;
			this._comments = null;
			this._posts = null;
		}
	}

	getCommentByPostId(postId: number) {
		if (postId) {
			this._homeService.findCommentByPostID(postId).subscribe((res: any) => {
				this._comments = res;
			});
		} else {
			this.noPostId = true;
			this._posts = null;
			this._comments = null;
		}
	}

	compareWithFn = (
		o1: { id: number; name: string },
		o2: { id: number; name: string }
	): boolean => {
		if (o1.id && o2.id) {
			return o1.id === o2.id;
		}

		return o1 === o2;
	};

	compareWith = this.compareWithFn;
}
