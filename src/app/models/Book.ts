export class Book {
	title: string;
	description: string;
	pageCount: number;
	excerpt: string;
	date: string;
}
