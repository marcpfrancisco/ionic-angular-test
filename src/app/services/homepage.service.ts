import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class HomepageService {
	baseUrl: string = "https://jsonplaceholder.typicode.com";
	limit: string = "?_limit=10";

	constructor(private _http: HttpClient) {}

	getAllUsers() {
		return this._http.get(`${this.baseUrl}/users`);
	}

	getAllPosts() {
		return this._http.get(`${this.baseUrl}/posts`);
	}

	findPostByUserId(userId) {
		if (!userId) return;
		return this._http.get(`${this.baseUrl}/posts?userId=${userId}`);
	}

	findCommentByPostID(postId) {
		if (!postId) return;
		return this._http.get(`${this.baseUrl}/comments?postId=${postId}`);
	}
}
